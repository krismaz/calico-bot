import math
from time import sleep
from typing import Any, List, Optional, Tuple

import matplotlib
matplotlib.use("GTK3Agg")

from matplotlib import patches
from matplotlib import pyplot as plt
from matplotlib import rcParams

from board import get_total_scores_from_partial
from hyperparams import FIELD_COORDS, GOAL_COORDS, NUM_FIELDS
from policy.interface import AbstractPolicy
from state import State
from tile import Color, Pattern, Tile

COLOR_MAP = {
    None: "black",
    Color.BLUE: "darkturquoise",
    Color.GREEN: "green",
    Color.NAVY: "navy",
    Color.PURPLE: "purple",
    Color.RED: "red",
    Color.YELLOW: "goldenrod",
}

HATCH_MAP = {
    Pattern.DOTS: ".",
    Pattern.FERNS: "x",
    Pattern.FLOWERS: "*",
    Pattern.LEAVES: "\\",
    Pattern.SQUARES: "+",
    Pattern.STRIPES: "|",
}


def plot_tile(
    ax: Any, center_x: float, center_y: float, tile: Tile, goal: Optional[str] = None
) -> None:
    if goal is not None:
        color = "black"
        hatch = None
    elif tile is None:
        color = "gray"
        hatch = None
    else:
        color = COLOR_MAP[tile.color]
        hatch = HATCH_MAP[tile.pattern]

    RADIUS = 0.5
    ANGLE = math.pi / 3.0
    ANGLES = [ANGLE / 2 + ANGLE * i for i in range(7)]

    vertices = [
        (RADIUS * math.cos(angle), RADIUS * math.sin(angle)) for angle in ANGLES
    ]

    ax.add_patch(
        patches.Polygon(
            [(center_x + x, center_y + y) for x, y in vertices],
            color=color,
            alpha=0.5,
            hatch=hatch,
        )
    )

    if goal:
        ax.text(center_x, center_y, goal.upper(), ha="center", va="center", c="white")


def plot_tiles_row(ax: Any, tiles: List[Tile]):
    ax.set_xlim((-0.8, 0.8))
    ax.set_ylim(-0.9, len(tiles) * 1.25 - 0.35)

    for idx, tile in enumerate(tiles):
        plot_tile(ax, 0.0, idx * 1.25, tile)


class Visualizer:
    def __init__(self, state: State):
        rcParams["font.family"] = "monospace"

        self.state = state
        self.fig, axes = plt.subplot_mosaic(
            "AABD\nAACD\nEECD",
            figsize=(10.55, 5.65),
            gridspec_kw={
                "height_ratios": [1.07, 0.8, 0.7],
                "width_ratios": [2.5, 1.5, 0.9, 1.6],
            },
        )

        self.axes = [axes[id] for id in ["A", "B", "C", "D", "E"]]

        [
            self.ax_board,
            self.ax_tiles_private,
            self.ax_tiles_shared,
            self.ax_score,
            self.ax_shape_targets,
        ] = self.axes

        self.fig.canvas.set_window_title("Calico")
        self.ax_board.invert_yaxis()

        plt.subplots_adjust(wspace=0.03)
        plt.ion()
        plt.show()

    def plot_state(self) -> None:
        self.ax_board.set_xlim((-1.6, 6.2))
        self.ax_board.set_ylim((-0.8, 6.0))
        self.ax_board.set_title("Board state")

        for idx in range(NUM_FIELDS):
            x_idx, y_idx = FIELD_COORDS[idx]

            x = y_idx - x_idx / 2
            y = math.sqrt(3) / 2 * x_idx

            tile = self.state.board.tiles[idx]
            pos = (x_idx, y_idx)

            if pos in GOAL_COORDS:
                goal = self.state.board.goals[GOAL_COORDS.index(pos)].design
            else:
                goal = None

            plot_tile(self.ax_board, x, y, tile, goal)

    def plot_scoring(self):
        self.ax_score.set_title("Scoring")
        partial_scores = self.state.board.get_partial_scores(self.state.shape_targets)

        def get_scores_per_type(source: str) -> List[Tuple[Any, List[int]]]:
            return sorted(
                partial_scores[source].per_type.items(), key=lambda tup: str(tup[0])
            )

        description = ""

        def add_description_row(label: str, value: str) -> None:
            nonlocal description
            description += label.ljust(15) + value.rjust(4) + "\n"

        for key, value in get_total_scores_from_partial(partial_scores).items():
            key = key.replace("_", " ")
            add_description_row(f"{key.capitalize()}:", f"{value}")

        description += "\n====== Goals ======\n"
        for goal, goal_partial_scores in get_scores_per_type("goals"):
            value_min, value_max = goal.value
            max_possible = max(goal_partial_scores)
            suffix = "?" if goal_partial_scores[0] != max_possible else ""

            add_description_row(
                f"{goal.design}({value_min}/{value_max}):", f"{max_possible}{suffix}"
            )

        description += "\n====== Colors ======\n"
        for color, color_partial_scores in get_scores_per_type("colors"):
            add_description_row(f"{color.name}:", f"{color_partial_scores[0]}")

        rainbow_bonus_score = partial_scores["rainbow_bonus"].per_type[0]
        add_description_row("Rainbow bonus:", f"{rainbow_bonus_score}")

        description += "\n====== Shapes ======\n"
        for shape_target, shape_partial_scores in get_scores_per_type("shapes"):
            add_description_row(
                f"{shape_target.shape_name}:", f"{shape_partial_scores[0]}"
            )

        self.ax_score.text(0.5, 0.94, description, ha="center", va="top")

    def plot_shape_targets(self):
        self.ax_shape_targets.set_xlim((-0.9, 6.9))
        self.ax_shape_targets.set_ylim((-0.8, 1.3))
        self.ax_shape_targets.set_title("Shape targets")

        x_curr = 0.0
        step = 1.2

        for shape_target in self.state.shape_targets:
            description = f"{shape_target.shape_name} ({shape_target.score})"
            self.ax_shape_targets.text(
                x_curr + step / 2.0, 1.0, description, ha="center", va="top"
            )

            for pattern in shape_target.patterns:
                plot_tile(
                    self.ax_shape_targets,
                    x_curr,
                    0.0,
                    Tile(color=None, pattern=pattern),
                )
                x_curr += step

    def plot_tiles_shared(self):
        self.ax_tiles_shared.set_title("Shared tiles")
        plot_tiles_row(self.ax_tiles_shared, self.state.tiles_shared)

    def plot_tiles_private(self):
        self.ax_tiles_private.set_title("Private tiles")
        plot_tiles_row(self.ax_tiles_private, self.state.tiles_private)

    def plot(self):
        for ax in self.axes:
            ax.clear()
            ax.set_xticks([])
            ax.set_yticks([])

        self.plot_state()
        self.plot_scoring()
        self.plot_shape_targets()
        self.plot_tiles_shared()
        self.plot_tiles_private()

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()


def visualize_game(policy: AbstractPolicy, sleep_duration: float = 0.75) -> None:
    visualizer = Visualizer(policy.state)
    visualizer.plot()

    while not policy.state.is_done():
        sleep(sleep_duration)

        policy.state.place_tile(*policy.step())
        policy.state.update_shared_tiles()

        visualizer.plot()

    plt.ioff()
    plt.show()
