from copy import deepcopy
from typing import Any, List, Set, Tuple, Union

from component import Component
from coord import Coord, normalize_coords, rotate_coords, shift_coords
from hyperparams import (
    COORDS_TO_INDEX,
    FIELD_COORDS,
    NEIGHBORS,
    SHAPE_TARGET_NAME_TO_PARAMS,
)
from tile import Pattern


class ShapeTarget:
    shape: Union[int, List[Coord]]
    shape_name: str
    patterns: List[Pattern]
    score: int

    def __init__(self, name: str, patterns: List[Pattern]) -> None:
        self.shape, self.shape_name, self.score = SHAPE_TARGET_NAME_TO_PARAMS[name]
        self.patterns = patterns

        if isinstance(self.shape, list):
            shape_current = deepcopy(self.shape)
            self.shape_variants = set()

            for _ in range(6):
                self.shape_variants.add(tuple(normalize_coords(shape_current)))
                shape_current = rotate_coords(shape_current)

    def get_partial_matches(self, components: List[Component]) -> List[int]:
        partial_matches = []

        if isinstance(self.shape, int):
            for component in components:
                if component.size_with_adjacent_empty >= self.shape:
                    partial_matches.append(max(self.shape - component.size, 0))
        else:
            field_to_comp_id = {}
            for component_id, component in enumerate(components):
                for field in component.coords:
                    field_to_comp_id[field] = component_id

                for field in component.coords_adjacent_empty:
                    field_to_comp_id[field] = None

            choices: List[Tuple[int, Set[Any]]] = []
            for anchor_x, anchor_y in FIELD_COORDS:
                for shape in self.shape_variants:
                    coords = shift_coords(shape, anchor_x, anchor_y)

                    num_missing = 0
                    component_ids = set()

                    for coord in coords:
                        if coord not in field_to_comp_id:
                            # Either `coord` is outside of the board, or is taken by a
                            # tile not in `components`.
                            num_missing = None
                            break
                        else:
                            component_id = field_to_comp_id[coord]
                            if component_id is None:
                                num_missing += 1
                            else:
                                component_ids.add(component_id)

                    if num_missing is None or not component_ids:
                        continue

                    choices.append((num_missing, component_ids | set(coords)))

            # Sort by the number of missing fields; break ties by the number of
            # different components / extra fields touched.
            choices.sort(key=lambda t: (t[0], len(t[1])))

            elements_used = set()
            for num_missing, elements in choices:
                overlaps = False
                for element in elements:
                    if element in elements_used:
                        overlaps = True
                        break

                    if isinstance(element, tuple):
                        neighbors = {
                            FIELD_COORDS[id]
                            for id in NEIGHBORS[COORDS_TO_INDEX[element]]
                        }

                        neighbor_comps = {
                            field_to_comp_id[neighbor]
                            for neighbor in neighbors
                            if neighbor in field_to_comp_id
                        }

                        if (neighbors | neighbor_comps) & elements_used:
                            overlaps = True
                            break

                if not overlaps:
                    partial_matches.append(num_missing)
                    elements_used |= elements

        return partial_matches
