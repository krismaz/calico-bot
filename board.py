from collections import defaultdict
from typing import Any, Dict, List, Optional, Union

from component import Component
from coord import Coord
from goal import Goal
from hyperparams import (
    FIELD_COORDS,
    GOAL_INDICES,
    MIN_COLOR_GROUP_SIZE,
    NEIGHBORS,
    NUM_FIELDS,
    SCORE_PER_COLOR_GROUP,
    SCORE_RAINBOW_BONUS,
)
from shape_target import ShapeTarget
from tile import Color, Tile


def is_on_border(idx: int) -> bool:
    return len(NEIGHBORS[idx]) < 6


class Scores:
    def __init__(self, per_type: Union[List[int], Dict[Any, List[int]]]):
        self.per_type = per_type

        if isinstance(per_type, list):
            self.total = per_type
        else:
            self.total = [0]

            for values in per_type.values():
                for idx, value in enumerate(values):
                    if idx < len(self.total):
                        self.total[idx] += value
                    else:
                        self.total.append(value)


def get_total_scores_from_partial(partial_scores: Dict[str, Scores]) -> Dict[str, int]:
    scores = {key: value.total[0] for key, value in partial_scores.items()}
    scores["total"] = sum(scores.values())

    return scores


class Board:
    tiles: List[Optional[Tile]]
    goals: List[Goal]

    def __init__(self, goals: List[Goal]):
        self.tiles = [None] * NUM_FIELDS
        self.goals = goals

    def place_tile(self, pos: int, tile: Tile) -> None:
        if pos in GOAL_INDICES:
            raise ValueError(
                f"Trying to place a tile at {FIELD_COORDS[pos]} which contains a goal."
            )

        if self.tiles[pos] is not None:
            raise ValueError(
                f"Trying to place a tile at {FIELD_COORDS[pos]} which contains another tile."
            )

        self.tiles[pos] = tile

    def get_free_positions(self) -> List[int]:
        return [
            idx
            for idx in range(NUM_FIELDS)
            if self.tiles[idx] is None and idx not in GOAL_INDICES
        ]

    def get_goals_scores(self) -> Scores:
        """Compute all (partial) scores related to goals.

        Returns:
            A mapping from `Goal`s to lists of 7 partial scores, represented as a
            `Scores` object. In each list, the `i`-th element scores satisfiable goals
            that are `i` fields short of being fully filled in.
        """
        NUM_TILES_AROUND_GOAL = 6
        scores = defaultdict(lambda: [0] * (NUM_TILES_AROUND_GOAL + 1))

        for goal, goal_pos in zip(self.goals, GOAL_INDICES):
            neighbor_tiles = [self.tiles[pos] for pos in NEIGHBORS[goal_pos]]
            neighbor_tiles = [tile for tile in neighbor_tiles if tile is not None]

            missing = NUM_TILES_AROUND_GOAL - len(neighbor_tiles)
            scores[goal][missing] += goal.get_max_possible_score(neighbor_tiles)

        return Scores(per_type=scores)

    def get_connected_components(self, attr: str) -> List[Component]:
        visited = [idx in GOAL_INDICES for idx in range(NUM_FIELDS)]
        values = [
            getattr(tile, attr) if tile is not None else None for tile in self.tiles
        ]

        def dfs(pos: int, target_value: Any, component: List[int]) -> None:
            visited[pos] = True
            component.append(pos)

            for neighbor in NEIGHBORS[pos]:
                if not visited[neighbor] and values[neighbor] == target_value:
                    dfs(neighbor, target_value, component)

        def map_to_coords(component: List[int]) -> List[Coord]:
            return [FIELD_COORDS[idx] for idx in component]

        components = []
        for pos in range(NUM_FIELDS):
            if self.tiles[pos] is not None and not visited[pos]:
                component = []
                dfs(pos, values[pos], component)

                component_adjacent_empty = []
                for pos_boundary in component:
                    dfs(pos_boundary, None, component_adjacent_empty)

                component_adjacent_empty = [
                    pos for pos in component_adjacent_empty if values[pos] is None
                ]

                for pos_adjacent_empty in component_adjacent_empty:
                    visited[pos_adjacent_empty] = False

                components.append(
                    Component(
                        value=values[pos],
                        coords=map_to_coords(component),
                        coords_adjacent_empty=map_to_coords(component_adjacent_empty),
                    )
                )

        return components

    def get_color_scores(self) -> Scores:
        """Compute all (partial) scores related to colors.

        Returns:
            A mapping from `Color`s to lists of `MIN_COLOR_GROUP_SIZE` partial scores,
            represented as a `Scores` object. In each list, the `i`-th element scores
            all components that are `i` fields short of `MIN_COLOR_GROUP_SIZE`.
        """
        components = self.get_connected_components(attr="color")
        scores = {color: [0] * MIN_COLOR_GROUP_SIZE for color in Color}

        for component in components:
            if component.size_with_adjacent_empty < MIN_COLOR_GROUP_SIZE:
                continue

            missing = max(MIN_COLOR_GROUP_SIZE - component.size, 0)
            scores[component.value][missing] += SCORE_PER_COLOR_GROUP

        return Scores(per_type=scores)

    def get_shape_scores(self, shape_targets: List[ShapeTarget]) -> Scores:
        """Compute all (partial) scores related to shape targets.

        Returns:
            A mapping from `ShapeTarget`s to lists of partial scores, represented as a
            `Scores` object. In each list, the `i`-th element scores all components that
            are `i` fields short of completing the pattern.
        """
        components = self.get_connected_components(attr="pattern")
        scores = {target: [0] for target in shape_targets}

        components_by_pattern = defaultdict(list)
        for component in components:
            components_by_pattern[component.value].append(component)

        for target in shape_targets:
            for pattern in target.patterns:
                for num_missing in target.get_partial_matches(
                    components_by_pattern[pattern]
                ):
                    if len(scores[target]) < num_missing + 1:
                        scores[target] += [0] * (num_missing + 1 - len(scores[target]))

                    scores[target][num_missing] += target.score

        return Scores(per_type=scores)

    def get_partial_scores(self, shape_targets: List[ShapeTarget]) -> Dict[str, Scores]:
        scores = {
            "goals": self.get_goals_scores(),
            "colors": self.get_color_scores(),
            "shapes": self.get_shape_scores(shape_targets),
        }

        min_tiles_for_rainbow = 0
        for partial_scores in scores["colors"].per_type.values():
            min_add = MIN_COLOR_GROUP_SIZE
            for idx, score in enumerate(partial_scores):
                if score > 0:
                    min_add = idx
                    break

            min_tiles_for_rainbow += min_add

        rainbow_bonus = [0] * (MIN_COLOR_GROUP_SIZE * len(Color) + 1)
        rainbow_bonus[min_tiles_for_rainbow] = SCORE_RAINBOW_BONUS

        scores["rainbow_bonus"] = Scores(per_type=rainbow_bonus)
        return scores

    def get_scores(self, shape_targets: List[ShapeTarget]) -> Dict[str, int]:
        return get_total_scores_from_partial(self.get_partial_scores(shape_targets))
