import math
import random
from collections import defaultdict
from typing import Callable, Iterable, List

import numpy as np
from tqdm import tqdm

from board import Board, is_on_border
from goal import Goal
from hyperparams import (
    GOAL_DESIGNS,
    MAX_NUM_OPPONENTS,
    MIN_NUM_OPPONENTS,
    NUM_FIELDS,
    NUM_GOALS,
    NUM_SHAPE_TARGETS_PER_GAME,
    NUM_TILES_PRIVATE,
    NUM_TILES_SHARED,
    SHAPE_TARGET_NAMES,
)
from policy.interface import AbstractPolicy
from shape_target import ShapeTarget
from state import State
from tile import Pattern, Tile


def generate_random_shape_targets(num_samples: int) -> List[ShapeTarget]:
    names = random.sample(SHAPE_TARGET_NAMES, k=num_samples)
    patterns = random.sample(list(Pattern), k=2 * num_samples)

    return [
        ShapeTarget(name=name, patterns=[pat_1, pat_2])
        for name, pat_1, pat_2 in zip(names, patterns[::2], patterns[1::2])
    ]


def generate_random_state(seed: int) -> State:
    random.seed(seed)

    goal_params = random.sample(GOAL_DESIGNS.items(), k=NUM_GOALS)

    board = Board(goals=[Goal(*params) for params in goal_params])
    shape_targets = generate_random_shape_targets(
        num_samples=NUM_SHAPE_TARGETS_PER_GAME
    )

    for idx in range(NUM_FIELDS):
        if is_on_border(idx):
            board.tiles[idx] = Tile.get_random()

    def get_random_tiles(k: int) -> List[Tile]:
        return [Tile.get_random() for _ in range(k)]

    return State(
        board=board,
        shape_targets=shape_targets,
        tiles_private=get_random_tiles(NUM_TILES_PRIVATE),
        tiles_shared=get_random_tiles(NUM_TILES_SHARED),
        num_opponents=random.randint(MIN_NUM_OPPONENTS, MAX_NUM_OPPONENTS),
    )


def evaluate(policy_fn: Callable[[], AbstractPolicy], seeds: Iterable[int]) -> None:
    num_games = 0
    scores = defaultdict(list)

    for seed in tqdm(seeds):
        num_games += 1
        state = generate_random_state(seed)

        policy = policy_fn(state)
        policy.rollout()

        for key, value in state.board.get_scores(state.shape_targets).items():
            scores[key].append(value)

    print(f"Done evaluating on {num_games} games.")

    header_length = max(len(key) for key in scores.keys()) + 3
    for key, values in scores.items():
        mean = np.mean(values)
        std = np.std(values)
        sem = std / math.sqrt(len(values))

        print(
            f"{'[' + key + ']':{header_length}s}: {mean:0.2f} (std {std:0.2f}, sem {sem:0.2f})"
        )

    print("")
