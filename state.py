import random
from typing import List, NamedTuple

from board import Board
from shape_target import ShapeTarget
from tile import Tile


class State(NamedTuple):
    board: Board
    shape_targets: List[ShapeTarget]
    tiles_private: List[Tile]
    tiles_shared: List[Tile]
    num_opponents: int

    def _replace_shared_tile(self, idx: int) -> None:
        self.tiles_shared[idx] = Tile.get_random()

    def place_tile(self, idx_private: int, pos: int, idx_shared_to_take: int) -> None:
        self.board.place_tile(pos=pos, tile=self.tiles_private[idx_private])
        self.tiles_private[idx_private] = self.tiles_shared[idx_shared_to_take]
        self._replace_shared_tile(idx_shared_to_take)

    def update_shared_tiles(self) -> None:
        for _ in range(self.num_opponents):
            self._replace_shared_tile(random.randint(0, len(self.tiles_shared) - 1))

    def is_done(self) -> None:
        return self.board.tiles.count(None) == len(self.board.goals)
