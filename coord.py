from typing import List, Tuple


Coord = Tuple[int, int]


def rotate_coords(coords: List[Coord]) -> List[Coord]:
    return [(y, y - x) for x, y in coords]


def shift_coords(coords: List[Coord], shift_x: int, shift_y: int) -> List[Coord]:
    return [(x + shift_x, y + shift_y) for x, y in coords]


def normalize_coords(coords: List[Coord]) -> List[Coord]:
    anchor_x, anchor_y = min(coords)
    return sorted(shift_coords(coords, -anchor_x, -anchor_y))
