import random
from copy import deepcopy
from typing import Optional

from hyperparams import NUM_TILES_PRIVATE, NUM_TILES_SHARED
from policy.interface import AbstractPolicy, Move


class GreedyPolicy(AbstractPolicy):
    def _score_move(self, move: Move) -> int:
        state_copy = deepcopy(self.state)
        state_copy.place_tile(*move)

        return state_copy.board.get_scores(state_copy.shape_targets)["total"]

    def step(self) -> Move:
        all_moves = []
        for idx_private in range(NUM_TILES_PRIVATE):
            for pos_in_board in self.state.board.get_free_positions():
                all_moves.append(
                    Move(
                        idx_private=idx_private,
                        pos_in_board=pos_in_board,
                        idx_shared_to_take=random.randint(0, NUM_TILES_SHARED - 1),
                    )
                )

        random.shuffle(all_moves)

        best_move: Optional[Move] = None
        best_score: Optional[float] = None

        for move in all_moves:
            score = self._score_move(move)
            if best_score is None or score > best_score:
                best_move, best_score = move, score

        return best_move
