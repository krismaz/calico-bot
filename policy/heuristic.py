from copy import deepcopy

from policy.greedy import GreedyPolicy
from policy.interface import Move


class HeuristicPolicy(GreedyPolicy):
    _COEFS = {
        "goals": [1.0, 0.1, 0.01, 0.001, 0.0001],
        "colors": [1.0, 0.3, 0.03],
        "shapes": [1.0, 0.2, 0.02, 0.002],
        "rainbow_bonus": [1.0, 0.1, 0.01],
    }

    def _score_move(self, move: Move) -> float:
        state_copy = deepcopy(self.state)
        state_copy.place_tile(*move)

        score = 0.0
        for key, values in state_copy.board.get_partial_scores(
            state_copy.shape_targets
        ).items():
            for coef, value in zip(self._COEFS[key], values.total):
                score += coef * value

        return score
