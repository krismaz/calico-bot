import random

from hyperparams import NUM_TILES_PRIVATE, NUM_TILES_SHARED
from policy.interface import AbstractPolicy, Move


class RandomPolicy(AbstractPolicy):
    def step(self) -> Move:
        return Move(
            idx_private=random.randint(0, NUM_TILES_PRIVATE - 1),
            pos_in_board=random.choice(self.state.board.get_free_positions()),
            idx_shared_to_take=random.randint(0, NUM_TILES_SHARED - 1),
        )
