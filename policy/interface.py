from abc import ABC, abstractmethod
from typing import NamedTuple

from state import State


class Move(NamedTuple):
    idx_private: int
    pos_in_board: int
    idx_shared_to_take: int


class AbstractPolicy(ABC):
    def __init__(self, state: State) -> None:
        self.state = state

    @abstractmethod
    def step(self) -> Move:
        raise NotImplementedError()

    def rollout(self) -> None:
        while not self.state.is_done():
            self.state.place_tile(*self.step())
            self.state.update_shared_tiles()
