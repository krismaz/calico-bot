from typing import Any, List, NamedTuple

from coord import Coord


class Component(NamedTuple):
    value: Any
    coords: List[Coord]
    coords_adjacent_empty: List[Coord]

    @property
    def size(self) -> int:
        return len(self.coords)

    @property
    def size_with_adjacent_empty(self) -> int:
        return len(self.coords) + len(self.coords_adjacent_empty)
