from __future__ import annotations

from collections import Counter
from typing import List, Tuple

from tile import Tile


def can_match_design(values: List[int], design: str) -> bool:
    value_counts = list(Counter(values).values())
    design_counts = list(Counter(design).values())

    if len(value_counts) > len(design_counts):
        return False

    value_counts = sorted(value_counts, reverse=True)
    design_counts = sorted(design_counts, reverse=True)

    for current, target in zip(value_counts, design_counts):
        if current > target:
            return False

    return True


class Goal:
    design: str
    value: Tuple[int, int]

    def __init__(self, design: str, value: Tuple[int, int]) -> None:
        self.design = design
        self.value = value

    def get_max_possible_score(self, tiles: List[Tile]) -> int:
        can_be_satisfied = 0

        for attr in ["color", "pattern"]:
            values = [getattr(tile, attr).value for tile in tiles]
            can_be_satisfied += can_match_design(values, self.design)

        if can_be_satisfied == 0:
            return 0
        else:
            return self.value[can_be_satisfied - 1]
