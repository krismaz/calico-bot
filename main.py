from argparse import ArgumentParser

from evaluate import evaluate, generate_random_state
from policy.greedy import GreedyPolicy
from policy.heuristic import HeuristicPolicy
from policy.random import RandomPolicy
from vis import visualize_game


SEED_OFFSETS = {"train": 2_000_000, "valid": 1_000_000, "test": 0}

POLICIES = {
    "random": RandomPolicy,
    "greedy": GreedyPolicy,
    "heuristic": HeuristicPolicy,
}


def get_argparser() -> ArgumentParser:
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest="command", required=True)

    parser_benchmark = subparsers.add_parser(
        "benchmark", help="Benchmark all available policies."
    )
    parser_benchmark.add_argument(
        "--num-games",
        type=int,
        default=10,
        help="Number of games to average over for each policy.",
    )

    parser_visualize = subparsers.add_parser(
        "visualize", help="Step through a single rollout for a given policy."
    )
    parser_visualize.add_argument(
        "--seed", type=int, default=0, help="Seed to use to generate the board."
    )
    parser_visualize.add_argument(
        "--policy",
        type=str,
        required=True,
        choices=POLICIES.keys(),
        help="Policy to use.",
    )

    return parser


def main():
    parser = get_argparser()
    args = parser.parse_args()

    if args.command == "benchmark":
        offset = SEED_OFFSETS["train"]
        for name, policy_fn in POLICIES.items():
            print(f"Evaluating policy: {name}")
            evaluate(policy_fn, range(offset, offset + args.num_games))
    elif args.command == "visualize":
        visualize_game(POLICIES[args.policy](generate_random_state(args.seed)))


if __name__ == "__main__":
    main()
