# Description of the board.
FIELD_COORDS = sum(
    [
        [(x, y) for y in range(y_start, y_start + num_fields)]
        for x, (y_start, num_fields) in enumerate(
            [(0, 6), (0, 7), (0, 7), (1, 7), (1, 7), (2, 7), (3, 6)]
        )
    ],
    [],
)
NUM_FIELDS = len(FIELD_COORDS)

COORDS_TO_INDEX = {pos: idx for idx, pos in enumerate(FIELD_COORDS)}

NEIGHBORS = [[] for _ in range(NUM_FIELDS)]
for pos, (x, y) in enumerate(FIELD_COORDS):
    for shift_x, shift_y in [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1)]:
        neighbor = (x + shift_x, y + shift_y)

        if neighbor in COORDS_TO_INDEX:
            NEIGHBORS[pos].append(COORDS_TO_INDEX[neighbor])

# Placement of goal tiles.
GOAL_COORDS = [(2, 3), (3, 5), (4, 3)]
NUM_GOALS = len(GOAL_COORDS)

GOAL_INDICES = [COORDS_TO_INDEX[coords] for coords in GOAL_COORDS]

# Scoring hyperparameters.
SCORE_PER_COLOR_GROUP = 3
MIN_COLOR_GROUP_SIZE = 3
SCORE_RAINBOW_BONUS = 3

# Shape targets actually used in Calico, together with their payoffs. Defined just to make
# entering these a bit easier.
SHAPE_TARGET_NAME_TO_PARAMS = {
    "millie": (3, "3x", 3),
    "tibbit": (4, "4x", 5),
    "coconut": (5, "5x", 7),
    "cira": (6, "6x", 9),
    "gwenivere": (7, "7x", 11),
    "callie": ([(0, 0), (0, 1), (1, 1)], "3-Triangle", 3),
    "rumi": ([(0, i) for i in range(3)], "3-Line", 5),
    "tecolote": ([(0, i) for i in range(4)], "4-Line", 7),
    "leo": ([(0, i) for i in range(5)], "5-Line", 11),
    "almond": ([(0, 0), (0, 1), (0, 2), (1, 1), (1, 2)], "3+2", 9),
}
SHAPE_TARGET_NAMES = list(SHAPE_TARGET_NAME_TO_PARAMS.keys())

# Available goals, together with their payoffs.
GOAL_DESIGNS = {
    "abcdef": (10, 15),
    "aaaabb": (8, 14),
    "aaabbb": (8, 13),
    "aaabbc": (7, 11),
    "aabbcc": (7, 11),
    "aabbcd": (7, 8),
}

# Game setup.
NUM_SHAPE_TARGETS_PER_GAME = 3

MIN_NUM_OPPONENTS = 1
MAX_NUM_OPPONENTS = 3

NUM_TILES_PRIVATE = 2
NUM_TILES_SHARED = 3
