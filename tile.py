from __future__ import annotations

import random

from enum import Enum
from typing import NamedTuple


class Color(Enum):
    BLUE = 0
    GREEN = 1
    NAVY = 2
    PURPLE = 3
    RED = 4
    YELLOW = 5


class Pattern(Enum):
    DOTS = 0
    FERNS = 1
    FLOWERS = 2
    LEAVES = 3
    STRIPES = 4
    SQUARES = 5


class Tile(NamedTuple):
    color: Color
    pattern: Pattern

    def get_random() -> Tile:
        return Tile(
            color=random.choice(list(Color)), pattern=random.choice(list(Pattern))
        )
